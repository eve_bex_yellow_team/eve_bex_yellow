Meet Eve is the web based application using Artificial Intelligence and Natural Language Processing. We have created a boot, Eve, that communicates with the child and guides
him/her whenever they feel anxious or just want to talk to someone. We have used the app to present the Adolescent Guide in an interactive way. However, this is just the start. 
Please check the video we have submitted to get further details on this. 

The app aims to simulate natural conversation, therefore while interacting with Eve the child can speak*1* or type the message and Eve will display the message 
(and if activated) communicate using natural language. 
*1* for the purpose of the 24 hours hack, voice recognition was deactivated

You can try yourself the app at [http://meet-eve.herokuapp.com/](Link URL). Mind to allow the mic to used by the app.
In Downloads section of this repository you will find the code in a .zip format.

And here is a video showing the app at work:

[![And here is a video showing the app at work](https://bitbucket.org/repo/x4baq6/images/788093251-Video.png)](https://youtu.be/E57nV7wtCwE "Meet Eve")

The app can be used across all sorts of desktop and mobile devices and it requires internet connection.
It has been developed by a 7 members team located in Bucharest:

* Cezar Sarbu
* Ciprian Pascu
* Constantin Aldea 
* Cristina Aciubotaritei 
* Stefan Polisevschi
* Petru Mot
* Petrina Costoiu

![Eve_team_3.jpg](https://bitbucket.org/repo/x4baq6/images/4246175275-Eve_team_3.jpg)

![Eve_team_2.jpg](https://bitbucket.org/repo/x4baq6/images/24714743-Eve_team_2.jpg)

![Eve_team_4.jpg](https://bitbucket.org/repo/x4baq6/images/3796630610-Eve_team_4.jpg)